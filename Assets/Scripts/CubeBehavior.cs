﻿using UnityEngine;

public class CubeBehavior : MonoBehaviour
{
    bool shouldAnimate;
    float StartY;
    void Start()
    {
        shouldAnimate = true;
         StartY = transform.position.y;
    }

    void FixedUpdate()
    {
        if (shouldAnimate)
        {
            Vector3 pos = transform.position;
            pos.y = StartY + Mathf.Sin(Time.time) * 0.7f;
            transform.position = pos;
            float scale = (0.5f + Mathf.Sin(Time.time) / 4.0f);
            transform.localScale = new Vector3(scale, scale, scale);
            transform.Rotate(new Vector3(-2, 2, 0), Space.Self);
        }
    }
}
