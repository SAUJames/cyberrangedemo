﻿using UnityEngine;

public class CylinderBehavior : MonoBehaviour
{
    bool shouldAnimate;
    bool switchFlip = false;

    void Start()
    {
        shouldAnimate = true;
    }

    void FixedUpdate()
    {
        if (shouldAnimate)
        {
            Vector3 boundsRight = new Vector3(4f, 1.6f, 21.9f);
            Vector3 boundsLeft = new Vector3(-4f, 1.589f, 21.822f);


            Debug.Log(transform.position);

            //transform.Rotate(new Vector3(-2, 2, 0), Space.Self);
            if(transform.position == boundsRight)
            {
                switchFlip = !switchFlip;
            }
            else if (transform.position == boundsLeft)
            {
                switchFlip = !switchFlip;
            }

            if (switchFlip == false)
            {
                transform.Translate(new Vector3(0.02f, 0f, 0f));
            }
            else if (switchFlip == true)
            {
                transform.Translate(new Vector3(-0.02f, 0f, 0f));
            }
        }
    }
}
