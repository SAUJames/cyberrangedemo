﻿using UnityEngine;

public class SphereBehavior : MonoBehaviour
{
    bool shouldAnimate;
    float startY;

    void Start()
    {
        shouldAnimate = true; 
        startY = transform.position.y;
    }

    void FixedUpdate()
    {
        if (shouldAnimate)
        {
            Vector3 pos = transform.position;
            pos.y = startY + Mathf.Sin(Time.time) * 0.7f;
            transform.position = pos;
        }
    }
}
